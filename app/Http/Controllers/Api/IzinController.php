<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DetailTanggalIzin;
use App\DetailFileIzin;
use App\Izin;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreIzin;


class IzinController extends Controller
{
	public function store(StoreIzin $request){
	    $izin = izin::Create([
	    	'id_user' => $request->id_user,
	    ]);

	    foreach ($request->tanggal as $key => $value) {
	    	$izin = DetailTanggalIzin::Create([
		    	'id_izin' => $request->id_user,
		    	'tanggal_izin' => $value,
		    ]);
	    }

	    foreach ($request->file as $key => $value) {
	    	$izin = DetailFileIzin::Create([
		    	'id_izin' => $request->id_user,
		    	'nama_file' => $value,
		    	'direktori_file' => $value,
		    ]);
	    }

	    return response()->json(
	    	[
	    		'success' => 'Image uploaded successfully'
	    	]
	    );  
	}

	public function list() {
		$results = DB::select('
			select i.*,f.*,t.* from izin i
			inner join users s on i.id_user = s.id
			left join detail_file_izin f on f.id_izin=i.id
			left join detail_tanggal_izin t on t.id_izin=i.id
		');

		return response()->json(
	    	[
	    		'success' => $results
	    	]
	    );  
	} 
	public function list_row($id) {
		$results = DB::select('
			select i.*,f.*,t.* from izin i
			inner join users s on i.id_user = s.id
			left join detail_file_izin f on f.id_izin=i.id
			left join detail_tanggal_izin t on t.id_izin=i.id
			where i.id=:id 
		', ['id'=>$id]);

		return response()->json(
	    	[
	    		'success' => $results
	    	]
	    );  
	} 
}
