<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTanggalIzin extends Model
{
    protected $table = 'detail_tanggal_izin';
    protected $fillable = [
        'id_izin','tanggal_izin'
    ];
}
