<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailFileIzin extends Model
{
    protected $table = 'detail_file_izin';
    protected $fillable = [
        'id_izin','nama_file','direktori_file'
    ];
}
