<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'bayu_herdiyanto',
            'email' => 'bayu.herdiyanto@gmail.com',
            'password' => bcrypt("12345"),
            'api_token' => Str::random(80),
        ]);
    }
}
